#include <stdio.h>
#include <cycle_count.h>
#include "picture.h"

#define COUNT 1

void write_picture(const char* file_name)
{
	FILE* output_file=NULL;

	output_file=fopen(file_name,"w");

	if(output_file!=NULL)
	{
		for(int i=0;i<HEIGHT;++i)
		{
			for(int j=0;j<WIDTH;++j)
			{
				fprintf(output_file,"%d\n",picture_matrix[i][j]);
			}
		}

		fclose(output_file);
	}
}

int main(int argc, char *argv[])
{
	int s=0;
	int average=BLOCK_SIZE*BLOCK_SIZE;

#ifdef COUNT
	cycle_t start_count;
	cycle_t final_count;

	START_CYCLE_COUNT(start_count);
#endif

	for(int i=0;i<HEIGHT;i=i+BLOCK_SIZE)
	{
		for(int j=0;j<WIDTH;j=j+BLOCK_SIZE)
		{
			s=0;
			for(int k=0;k<BLOCK_SIZE;++k)
			{
				for(int l=0;l<BLOCK_SIZE;++l)
				{
					s=s+picture_matrix[i+k][j+l];
				}
			}

			s=s/average;

			for(int k=0;k<BLOCK_SIZE;++k)
			{
				for(int l=0;l<BLOCK_SIZE;++l)
				{
					picture_matrix[i+k][j+l]=s;
				}
			}
		}
	}
#ifdef COUNT
	STOP_CYCLE_COUNT(final_count,start_count);
	PRINT_CYCLES("Broj ciklusa: ",final_count);
#endif

	write_picture("output.txt");

	return 0;
}


