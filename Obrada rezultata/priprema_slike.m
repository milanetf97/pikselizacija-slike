%UCITAVANJE SLIKE

%Ime slike i format
NAME = "lena.jpg";
FMT = "jpg";

%Header koji ce se praviti
HEADER_FILE = "picture.h";

%Ucitavanje slike
I = imread(NAME,FMT);

figure();
imshow(I);

%Pretvaranje u grayscale sliku
GRAY = rgb2gray(I);

gray_size = size(GRAY);

%PRAVLJENJE HEDER FAJLA

%Promjena dimenzija slike radi upisa
image1D = reshape(GRAY.',1,[]);

%Otvaranje fajla
FILE = fopen(HEADER_FILE, 'w');

%Upis
fprintf(FILE,"%s %d\n","#define HEIGHT ",gray_size(1));
fprintf(FILE,"%s %d\n","#define WIDTH ",gray_size(2));
fprintf(FILE,"%s %d\n","#define BLOCK_SIZE ",4);
fprintf(FILE,"%s\n","#pragma section(""seg_sdram"")");
fprintf(FILE,"%s","int picture_matrix[HEIGHT][WIDTH]={");
for i=1:gray_size(1)*gray_size(2)-1
    fprintf(FILE,"%d,",image1D(i));
end
fprintf(FILE,"%d};\n",image1D(gray_size(1)*gray_size(2)));
fclose(FILE);
