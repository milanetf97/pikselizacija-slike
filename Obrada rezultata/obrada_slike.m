%Ime txt fajla sa slikom
NAME = "output.txt";
ORIGINAL = "lena.jpg";
FMT = "jpg";

%Citaj original
%Ucitavanje slike
I = imread(ORIGINAL,FMT);

%Pretvaranje u grayscale sliku
GRAY = rgb2gray(I);

gray_size = size(GRAY);

%----------------------------------------
%Pikselizacija originalne slike u MATLABU
%----------------------------------------

%Odredjivanje velicine za smanjenje radi pikselizacije 4x4
SCALE = size(GRAY)/4;

%Smanjivanje slike
NEW_IM = imresize(GRAY,SCALE,'box');

%Vracanje na originalne dimenzije, pikeslizovana slika
NEW_IM = imresize(NEW_IM,gray_size,'nearest');

%Prikaz originalne slike pikselizovane u MATLAB-u
figure();
imshow(NEW_IM);
title("ORIGINALNA SLIKA PIKSELIZOVANA");


%------------------------------------------
%Ucitavanje i priprema za analizu DSP slike
%------------------------------------------

%Citanje obradjene slike
FILE = fopen(NAME,'r');
PICTURE = fscanf(FILE,"%d");

%Reshape jednodimenzionalnog niza PICTURE koji je procitan iz fajla u 
% matricu i kastovanje matrice u cjelobrojni tip da bi se mogla naci
% slika razlike
PICTURE_N=cast(transpose(reshape(PICTURE,gray_size(1),[])),'uint8');

fclose(FILE);

%Prikaz slike obradjene na DSP-ju
figure();
imshow(mat2gray(PICTURE_N,[0 255]));
title("DSP SLIKA");

%----------------------------------------
%Pronalazenje slike razlike
%----------------------------------------

%Slika razlike
DIFF = NEW_IM - PICTURE_N;

%Prikaz slike razlike
figure();
imshow(DIFF);
title("SLIKA RAZLIKE");