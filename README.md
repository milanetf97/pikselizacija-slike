Projektni zadatak iz Sistema za digitalnu obradu signala 2022

Tema projektnog zadatka je pikselizacija slike na ADSP-21489 EZ-Kit Lite razvojnoj ploci u Cross Core Embedded Studio razvojnom okruzenju.
Slika je prethodno pripremljena za obradu u MATLAB-u.
Nakon obrade dobijeni rezultati su poredjeni sa referentnim rezultatim dobijenim u MATLAB-u.